package org.frknkrc44.ntp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;
import org.frknkrc44.ntp.SntpClientHandler.ClockTaskExecutor;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
		final SntpClientHandler handler = new SntpClientHandler(this);
		ClockTaskExecutor exec = new ClockTaskExecutor(){
			@Override
			public void onFinish(Boolean val){
				Toast.makeText(MainActivity.this,"time: "+val+", now: "+handler.getNow(),Toast.LENGTH_LONG).show();
			}
		};
		handler.requestTime(exec);
    }
}
